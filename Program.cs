﻿using System;
using System.IO;
using System.Collections.Generic;
using McMaster.Extensions.CommandLineUtils;
using MasterCard.Core;
using MasterCard.Core.Exceptions;
using MasterCard.Core.Model;
using MasterCard.Core.Security.OAuth;
using MasterCard.Api.Blockchain;
using Google.Protobuf;
using P001;

namespace csharp_blockchain_barebones_client
{
  class Program
  {
    public static String HELP_TEXT = "";
    static int Main(string[] args)
    {
      var app = new CommandLineApplication();
      app.HelpOption("-h|--help");
      var kpOption = app.Option("-kp|--keystorePath <keystorepath>", "the path to your keystore (mastercard developers)", CommandOptionType.SingleValue);
      var spOption = app.Option("-sp|--storePass <storepassword>", "keystore password (mastercard developers)", CommandOptionType.SingleValue);
      var kaOption = app.Option("-ka|--keyAlias <keyalias>", "key alias (mastercard developers)", CommandOptionType.SingleValue);
      var ckOption = app.Option("-ck|--consumerKey <consumerkey>", "consumer key (mastercard developers)", CommandOptionType.SingleValue);
      var pfOption = app.Option("-pf|--protoFile <protofile>", "the path to the protobuf file", CommandOptionType.SingleValue);
      var vOption = app.Option("-v|--verbosity", "log mastercard developers sdk to console", CommandOptionType.NoValue);
      app.OnExecute(() =>
      {
        var kp = kpOption.HasValue() ? kpOption.Value() : null;
        var sp = spOption.HasValue() ? spOption.Value() : "keystorepassword";
        var ka = kaOption.HasValue() ? kaOption.Value() : "keyalias";
        var ck = ckOption.HasValue() ? ckOption.Value() : null;
        var pf = pfOption.HasValue() ? pfOption.Value() : "message.proto";
        var v = vOption.HasValue();
        new BlockchainConsoleApp().start(kp, sp, ka, ck, pf, v);
      });
      HELP_TEXT = app.GetHelpText();
      return app.Execute(args);
    }
  }

  public delegate bool validate(String input);

  class BlockchainConsoleApp
  {
    public static readonly String APPID = "P001";
    public static readonly String ENCODING = "base64";

    String ks, kp, ka, ck, pf;
    bool v;
    void initAPI()
    {
      Console.WriteLine("Initializing");
      String ks = getFilePath("the path to your keystore (mastercard developers)", this.ks);
      String kp = getInput("keystore password (mastercard developers)", this.kp);
      String ka = getInput("key alias (mastercard developers)", this.ka);
      String ck = getInput("consumer key (mastercard developers)", this.ck);
      String pf = getFilePath("the path to the protobuf File", this.pf);
      ApiConfig.SetAuthentication(new OAuthAuthentication(ck, ks, ks, kp));
      ApiConfig.SetDebug(this.v);
      ApiConfig.SetSandbox(true);
      Console.WriteLine("Initialized");
    }

    String getInput(String msg, String defaultValue)
    {
      return getInput(msg, defaultValue, (input) =>
      {
        var exists = input != null && 0 < input.Length;
        if (!exists)
        {
          Console.WriteLine("Please enter a valid value");
        }
        return exists;
      });
    }
    String getInput(String msg, String defaultValue, validate callback)
    {
      String ret = null;
      String prompt = null == defaultValue ? $"{msg}:" : $"{msg} (:{defaultValue}):";
      do
      {
        Console.Write(prompt);
        ret = Console.ReadLine();
        if (0 == ret.Length && null != defaultValue)
        {
          ret = defaultValue;
        }
      } while (!callback(ret));
      return ret;
    }

    public void waitForEnter()
    {
      getInput("Press ENTER to continue", null, (input) => { return true; });
    }

    String getFilePath(String msg, String defaultValue)
    {
      return getInput(msg, defaultValue, (input) =>
      {
        var exists = File.Exists(input);
        if (!exists)
        {
          Console.WriteLine("Please enter a valid file");
        }
        return exists;
      });
    }

    public byte[] hexStringToBytes(string str)
    {
      var bytes = new byte[str.Length / 2];
      for (int idx = 0; idx < bytes.Length; idx++)
      {
        string currentHex = str.Substring(idx * 2, 2);
        bytes[idx] = Convert.ToByte(currentHex, 16);
      }
      return bytes;
    }

    public void showMenu()
    {
      bool done = false;
      do
      {
        Console.WriteLine();
        Console.WriteLine(File.ReadAllText("menu.txt"));
        var option = getInput("Option", "0", (input) =>
        {
          return Int32.Parse(input) >= 0 && Int32.Parse(input) <= 8;
        });
        switch (option)
        {
          case "0":
            done = true;
            break;
          case "1":
            updateNode();
            break;
          case "2":
            createEntity();
            break;
          case "3":
            retrieveEntity();
            break;
          case "4":
            retrieveBlock();
            break;
          case "5":
            retrieveLastConfirmedBlock();
            break;
          case "6":
            Console.WriteLine(File.ReadAllText(this.pf));
            break;
          case "7":
            initAPI();
            break;
          case "8":
            Console.WriteLine(Program.HELP_TEXT);
            break;
          default:
            Console.WriteLine("Unknown option");
            break;
        }
        if (!done)
        {
          waitForEnter();
        }
      } while (!done);
      Console.WriteLine("Goodbye");
    }
    public void updateNode()
    {
      Console.WriteLine("updateNode");
      String pf = getFilePath("Protobuf File", this.pf);
      try
      {
        RequestMap map = new RequestMap();
        map.Set("id", APPID);
        map.Set("name", APPID);
        map.Set("description", "");
        map.Set("version", 0);
        map.Set("definition.format", "proto3");
        map.Set("definition.encoding", ENCODING);
        map.Set("definition.messages", Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(File.ReadAllText(pf))));
        App app = new App(map).Update();
        Console.WriteLine("app--> {0}", app);
      }
      catch (ApiException e)
      {
        Console.Error.WriteLine("HttpStatus: {0}", e.HttpStatus);
        Console.Error.WriteLine("Message: {0}", e.Message);
        Console.Error.WriteLine("ReasonCode: {0}", e.ReasonCode);
        Console.Error.WriteLine("Source: {0}", e.Source);
      }
    }
    public void createEntity()
    {
      Console.WriteLine("createEntity");
      var txt = getInput("textEntry", "Hi There!");
      try
      {
        Message msg = new Message
        {
          Text = txt
        };
        RequestMap map = new RequestMap();
        map.Set("app", APPID);
        map.Set("encoding", ENCODING);
        map.Set("value", msg.ToByteString().ToBase64());
        TransactionEntry response = TransactionEntry.Create(map);
        Console.WriteLine("hash--> {0}", response["hash"]);
        Console.WriteLine("slot--> {0}", response["slot"]);
        Console.WriteLine("status--> {0}", response["status"]);
      }
      catch (ApiException e)
      {
        Console.Error.WriteLine("HttpStatus: {0}", e.HttpStatus);
        Console.Error.WriteLine("Message: {0}", e.Message);
        Console.Error.WriteLine("ReasonCode: {0}", e.ReasonCode);
        Console.Error.WriteLine("Source: {0}", e.Source);
      }
    }
    public void retrieveEntity()
    {
      Console.WriteLine("retrieveEntity");
      var hash = getInput("hash", null);
      try
      {
        RequestMap map = new RequestMap();
        map.Set("hash", hash);
        TransactionEntry response = TransactionEntry.Read("", map);

        Console.WriteLine("hash--> {0}", response["hash"]);
        Console.WriteLine("slot--> {0}", response["slot"]);
        Console.WriteLine("status--> {0}", response["status"]);
        Console.WriteLine("value--> {0}", response["value"]);
        Console.WriteLine("Decoded--> {0}", Message.Parser.ParseFrom(hexStringToBytes(response["value"].ToString())).ToString());
      }
      catch (ApiException e)
      {
        Console.Error.WriteLine("HttpStatus: {0}", e.HttpStatus);
        Console.Error.WriteLine("Message: {0}", e.Message);
        Console.Error.WriteLine("ReasonCode: {0}", e.ReasonCode);
        Console.Error.WriteLine("Source: {0}", e.Source);
      }
    }
    public void retrieveBlock()
    {
      Console.WriteLine("retrieveBlock");
      var blockId = getInput("blockId", null);
      try
      {
        Block response = Block.Read(blockId);
        Console.WriteLine("authority--> {0}", response["authority"]);
        Console.WriteLine("nonce--> {0}", response["nonce"]);
        Console.WriteLine("count {0}", response["partitions"]);
        Console.WriteLine("partitions[0].application--> {0}", response["partitions[0].application"]);
        Console.WriteLine("partitions[0].entries[0]--> {0}", response["partitions[0].entries[0]"]);
        Console.WriteLine("partitions[0].entry_count--> {0}", response["partitions[0].entry_count"]);
        Console.WriteLine("partitions[0].merkle_root--> {0}", response["partitions[0].merkle_root"]);
        Console.WriteLine("previous_block--> {0}", response["previous_block"]);
        Console.WriteLine("signature--> {0}", response["signature"]);
        Console.WriteLine("slot--> {0}", response["slot"]);
        Console.WriteLine("version--> {0}", response["version"]);
      }
      catch (ApiException e)
      {
        Console.Error.WriteLine("HttpStatus: {0}", e.HttpStatus);
        Console.Error.WriteLine("Message: {0}", e.Message);
        Console.Error.WriteLine("ReasonCode: {0}", e.ReasonCode);
        Console.Error.WriteLine("Source: {0}", e.Source);
      }
    }
    public void retrieveLastConfirmedBlock()
    {
      Console.WriteLine("retrieveLastConfirmedBlock");
      try
      {
        List<Block> responseList = Block.List();
        Block response = responseList[0];
        Console.WriteLine("authority--> {0}", response["authority"]);
        Console.WriteLine("hash--> {0}", response["hash"]);
        Console.WriteLine("nonce--> {0}", response["nonce"]);
        Console.WriteLine("previous_block--> {0}", response["previous_block"]);
        Console.WriteLine("signature--> {0}", response["signature"]);
        Console.WriteLine("slot--> {0}", response["slot"]);
        Console.WriteLine("version--> {0}", response["version"]);
      }
      catch (ApiException e)
      {
        Console.Error.WriteLine("HttpStatus: {0}", e.HttpStatus);
        Console.Error.WriteLine("Message: {0}", e.Message);
        Console.Error.WriteLine("ReasonCode: {0}", e.ReasonCode);
        Console.Error.WriteLine("Source: {0}", e.Source);
      }
    }
    public void start(String ks, String kp, String ka, String ck, String pf, bool v)
    {
      this.ks = ks; this.kp = kp; this.ka = ka; this.ck = ck; this.pf = pf; this.v = v;
      Console.WriteLine();
      Console.WriteLine(File.ReadAllText("help.txt"));
      initAPI();
      updateNode();
      showMenu();
    }

  }


}
